import xlrd

from database import Session
from models import SpimexTradingResults


def read_xls_file(file_path):
    """
    Читает данные из файла формата XLS и сохраняет их в базу данных.

    Args:
        file_path (str): Путь к файлу XLS.

    """
    session = Session()
    workbook = xlrd.open_workbook(file_path)
    sheet = workbook.sheet_by_index(0)

    date = sheet.cell(3, 1).value[13:] # Берем дату из ячейки B4
    for row in range(8, sheet.nrows): # начинаем читать данные с 9 строки
        row_data = sheet.row_values(row)
        if row_data[2] and row_data[14]!= '-' and int(row_data[14]) > 0:

            trading_result = SpimexTradingResults(
                exchange_product_id = row_data[1],  
                exchange_product_name = row_data[2],
                oil_id = row_data[1][:4],
                delivery_basis_id = row_data[1][4:7],
                delivery_basis_name = row_data[3],
                delivery_type_id = row_data[1][-1],
                volume = row_data[4],
                total = row_data[5], 
                count = row_data[14],
                date = date
            )
            
            session.add(trading_result)

    session.commit()