import os

from database import BaseModel, engine
from processing import read_xls_file


def main():
    """
    Читает файлы Excel из каталога 'parsed_data' и обрабатывает их для обновления базы данных.

    Эта функция перебирает все файлы с расширением '.xls' в каталоге 'parsed_data'.
    Для каждого файла вызывается функция 'read_xls_file' для извлечения необходимых данных и обновления базы данных.

    """
    BaseModel.metadata.create_all(engine)
    script_dir = os.path.dirname(__file__) 
    data_dir = os.path.join(script_dir, 'parsed_data')
    for root, _, files in os.walk(data_dir):
        for file in files:
            file_path = os.path.join(root, file)
            read_xls_file(file_path)

if __name__ == "__main__":
    main()