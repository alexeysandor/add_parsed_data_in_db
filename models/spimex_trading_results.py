from sqlalchemy import Column, Integer, String, Date, BigInteger

from datetime import datetime
from database import BaseModel

class SpimexTradingResults(BaseModel):
    __tablename__ = 'spimex_trading_results'

    id = Column(Integer, primary_key=True)
    exchange_product_id = Column(String)
    exchange_product_name = Column(String)
    oil_id = Column(String)
    delivery_basis_id = Column(String)
    delivery_basis_name = Column(String)
    delivery_type_id = Column(String)
    volume = Column(Integer)
    total = Column(BigInteger)
    count = Column(Integer)
    date = Column(Date)
    created_on = Column(Date, default=datetime.today())
    updated_on = Column(Date, nullable=True)
